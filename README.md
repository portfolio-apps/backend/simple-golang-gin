## Simple Golang Gin 

The below structure is the best practices structure for a Golang Gin project. These files were generated using OpenAI ChatGPT. The goal will be to structure this simple project like it's displayed below.

<pre>
simple-golang-gin
├── cmd
│   └── main.go
├── internal
│   ├── auth
│   ├── database
│   ├── logger
│   ├── server
│   ├── swagger
│   └── users
├── pkg
│   ├── auth
│   ├── database
│   ├── logger
│   ├── server
│   ├── swagger
│   └── users
├── test
│   ├── auth_test.go
│   ├── database_test.go
│   ├── logger_test.go
│   ├── server_test.go
│   ├── swagger_test.go
│   └── users_test.go
├── .gitlab-ci.yml
├── Dockerfile
├── docker-compose.yml
├── deployment.yml
├── ingress.yml
└── service.yml
</pre>