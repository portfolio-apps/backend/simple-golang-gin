package main

import (
	"fmt"
	"net/http"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

const (
	// Replace these values with your own secret key and issuer
	secretKey = "my-secret-key"
	issuer = "http://localhost:5000"
)

// User represents a user in the system
type User struct {
	Email string `json:"email"`
	Password string `json:"password"`
}

func main() {
	r := gin.Default()

	r.LoadHTMLGlob("*.html")
	// Default HTML route
	r.GET("/", func(c *gin.Context) {
		c.HTML(200, "index.html", gin.H{
			"title": "Welcome to boilerplate-app",
		})
	})

	// Unauthenticated "/ping" route
	r.GET("/ping", func(c *gin.Context) {
		c.String(200, "pong")
	})

	// "/register" route
	r.POST("/register", func(c *gin.Context) {
		// Add logic for registering a user
		c.String(200, "User has been registered")
	})

	// "/login" route
	r.POST("/login", func(c *gin.Context) {
		var user User
		if err := c.ShouldBindJSON(&user); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		if user.Email != "john@test.com" || user.Password != "test1234" {
			c.JSON(http.StatusUnauthorized, gin.H{"error": "Invalid email or password"})
			return
		}

		// Create a new JWT with the appropriate claims
		token := jwt.New(jwt.SigningMethodHS256)
		claims := token.Claims.(jwt.MapClaims)
		claims["email"] = user.Email
		claims["iss"] = issuer
		tokenString, err := token.SignedString([]byte(secretKey))
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}
		c.JSON(http.StatusOK, gin.H{"token": tokenString})
	})

	r.GET("/auth", func(c *gin.Context) {
		// Get the JWT from the header
		tokenString := c.GetHeader("Authorization")
		if tokenString == "" {
			c.JSON(http.StatusUnauthorized, gin.H{"error": "Token missing"})
			return
		}

		// Validate and decode the JWT
		token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
			}
			return []byte(secretKey), nil
		})
		if err != nil || !token.Valid {
			c.JSON(http.StatusUnauthorized, gin.H{"error": "Invalid token"})
			return
		}

		// Print the token claims to the console
		fmt.Println(token.Claims)

		c.JSON(http.StatusOK, gin.H{"message": "Token is valid"})
	})

	r.Run(":8000")
}
