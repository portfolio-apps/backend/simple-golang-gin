package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

var r *gin.Engine

func init() {
	gin.SetMode(gin.TestMode)
}

func TestRootRoute(t *testing.T) {
	// Create a new request to the "/" route
	req, _ := http.NewRequest("GET", "/", nil)
	// Create a new response recorder to record the response
	res := httptest.NewRecorder()
	// Call the router with the request and response recorder
	r.ServeHTTP(res, req)

	// Assert that the status code is 200 OK
	assert.Equal(t, 200, res.Code, "Incorrect status code")
	// Assert that the response body contains the expected HTML
	assert.Contains(t, res.Body.String(), "<title>Welcome to boilerplate-app</title>", "Unexpected response body")
}

func TestPingRoute(t *testing.T) {
	// Create a new request to the "/ping" route
	req, _ := http.NewRequest("GET", "/ping", nil)
	// Create a new response recorder to record the response
	res := httptest.NewRecorder()
	// Call the router with the request and response recorder
	r.ServeHTTP(res, req)

	// Assert that the status code is 200 OK
	assert.Equal(t, 200, res.Code, "Incorrect status code")
	// Assert that the response body is "pong"
	assert.Equal(t, "pong", res.Body.String(), "Unexpected response body")
}

func TestRegisterRoute(t *testing.T) {
	// Create a new request to the "/register" route with a JSON body
	req, _ := http.NewRequest("POST", "/register", strings.NewReader(`{"email": "test@example.com", "password": "password123"}`))
	req.Header.Set("Content-Type", "application/json")
	// Create a new response recorder to record the response
	res := httptest.NewRecorder()
	// Call the router with the request and response recorder
	r.ServeHTTP(res, req)

	// Assert that the status code is 200 OK
	assert.Equal(t, 200, res.Code, "Incorrect status code")
	// Assert that the response body is "User has been registered"
	assert.Equal(t, "User has been registered", res.Body.String(), "Unexpected response body")
}

func TestLoginRouteValidCredentials(t *testing.T) {
	// Create a new request to the "/login" route with a JSON body
	req, _ := http.NewRequest("POST", "/login", strings.NewReader(`{"email": "john@test.com", "password": "test1234"}`))
	req.Header.Set("Content-Type", "application/json")
	// Create a new response recorder to record the response
	res := httptest.NewRecorder()
	// Call the router with the request and response recorder
	r.ServeHTTP(res, req)

	// Assert that the status code is 200 OK
	assert.Equal(t, 200, res.Code, "Incorrect status code")

	// Parse the response body as JSON
	var jsonResponse map[string]string
	err := json.Unmarshal(res.Body.Bytes(), &jsonResponse)
	assert.Nil(t, err, "Error parsing JSON response")

	// Assert that the response contains a valid JWT
	_, err = jwt.Parse(jsonResponse["token"], func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(secretKey), nil
	})
	assert.Nil(t, err, "Error parsing JWT")
}

func TestLoginRouteInvalidCredentials(t *testing.T) {
	// Create a new request to the "/login" route with a JSON body
	req, _ := http.NewRequest("POST", "/login", strings.NewReader(`{"email": "invalid@test.com", "password": "invalid123"}`))
	req.Header.Set("Content-Type", "application/json")
	// Create a new response recorder to record the response
	res := httptest.NewRecorder()
	// Call the router with the request and response recorder
	r.ServeHTTP(res, req)

	// Assert that the status code is 401 Unauthorized
	assert.Equal(t, 401, res.Code, "Incorrect status code")

	// Parse the response body as JSON
	var jsonResponse map[string]string
	err := json.Unmarshal(res.Body.Bytes(), &jsonResponse)
	assert.Nil(t, err, "Error parsing JSON response")

	// Assert that the response contains the expected error message
	assert.Equal(t, "Invalid email or password", jsonResponse["error"], "Unexpected response body")
}
/**
func TestAuthRouteValidJWT(t *testing.T) {
	// Create a new request to the "/login" route with a JSON body to get a valid JWT
	req, _ := http.NewRequest("POST", "/login", strings.NewReader(`{"email": "john@test.com", "password": "test1234"}`))
	req.Header.Set("Content-Type", "application/json")
	// Create a new response recorder to record the response
	res := httptest.NewRecorder()
	// Call the router with the request and response recorder
	r.ServeHTTP(res, req)

	// Parse the response body as JSON to get the JWT
	var jsonResponse map[string]string
	err := json.Unmarshal(res.Body.Bytes(), &jsonResponse)
	assert.Nil(t, err, "Error parsing JSON response")
	token := jsonResponse["token"]

	// Create a new request to the "/auth" route with the JWT in the header
	req, _ = http.NewRequest("GET", "/auth", nil)
	req.Header.Set("Authorization", "Bearer "+token)
	// Create a new response recorder to record the response
	res = httptest.NewRecorder()
	// Call the router with the request and response recorder
	r.ServeHTTP(res, req)

	// Assert that the status code is 200 OK
	assert.Equal(t, 200, res.Code, "Incorrect status code")

	// Parse the response body as JSON
	err = json.Unmarshal(res.Body.Bytes(), &jsonResponse)
	assert.Nil(t, err, "Error parsing JSON response")

	// Assert that the response contains the expected message
	assert.Equal(t, "Token is valid", jsonResponse["message"], "Unexpected response body")
} 
*/

func TestAuthRouteInvalidJWT(t *testing.T) {
	// Create a new request to the "/auth" route with an invalid JWT in the header
	req, _ := http.NewRequest("GET", "/auth", nil)
	req.Header.Set("Authorization", "Bearer invalid.token")
	// Create a new response recorder to record the response
	res := httptest.NewRecorder()
	// Call the router with the request and response recorder
	r.ServeHTTP(res, req)

	// Assert that the status code is 401 Unauthorized
	assert.Equal(t, 401, res.Code, "Incorrect status code")

	// Parse the response body as JSON
	var jsonResponse map[string]string
	err := json.Unmarshal(res.Body.Bytes(), &jsonResponse)
	assert.Nil(t, err, "Error parsing JSON response")

	// Assert that the response contains the expected error message
	assert.Equal(t, "Invalid token", jsonResponse["error"], "Unexpected response body")
}

func loginHandler(c *gin.Context) {
	var user User
	if err := c.ShouldBindJSON(&user); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if user.Email != "john@test.com" || user.Password != "test1234" {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Invalid email or password"})
		return
	}

	// Create a new JWT with the appropriate claims
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)
	claims["email"] = user.Email
	claims["iss"] = issuer
	tokenString, err := token.SignedString([]byte(secretKey))
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"token": tokenString})
}

func authHandler(c *gin.Context) {
	// Get the JWT from the header
	tokenString := c.GetHeader("Authorization")
	if tokenString == "" {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Token missing"})
		return
	}

	// Validate and decode the JWT
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(secretKey), nil
	})
	if err != nil || !token.Valid {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Invalid token"})
		return
	}

	// Print the token claims to the console
	fmt.Println(token.Claims)

	c.JSON(http.StatusOK, gin.H{"message": "Token is valid"})
}

func TestMain(m *testing.M) {
	// Set up the router for testing
	r = gin.Default()
	r.LoadHTMLGlob("*.html")
	// Define the routes for the router
	r.GET("/", func(c *gin.Context) {
		c.HTML(200, "index.html", gin.H{
			"title": "Welcome to boilerplate-app",
		})
	})
	r.GET("/ping", func(c *gin.Context) {
		c.String(200, "pong")
	})
	r.POST("/register", func(c *gin.Context) {
		c.String(200, "User has been registered")
	})
	r.POST("/login", loginHandler)
	r.GET("/auth", authHandler)
	// Run the tests
	m.Run()
}